package com.epam

import com.holdenkarau.spark.testing.SharedSparkContext
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * @author Dmitrii_Kniazev 
  * @since 12/07/2016
  */
@RunWith(classOf[JUnitRunner])
class SparkAppTest extends FunSuite with SharedSparkContext {

  val INPUT_LIST = List(
    "ip1566 - - [27/Apr/2011:22:28:50 -0400] \"HEAD /sunFAQ/serial/ HTTP/1.0\" 200 512 \"-\" \"nph-churl\"",
    "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"",
    "ip46 - - [27/Apr/2011:22:24:05 -0400] \"GET /sunFAQ/faq_framebuffer/framebuffer.html HTTP/1.1\" 304 - \"-\" \"Sogou web spider/4.0(+http://www.sogou.com/docs/help/webmasters.htm#07)\"",
    "ip1565 - - [27/Apr/2011:22:27:27 -0400] \"GET /sun_ipx/ HTTP/1.1\" 200 11586 \"-\" \"MLBot (www.metadatalabs.com/mlbot)\"",
    "ip1566 - - [27/Apr/2011:22:28:50 -0400] \"HEAD /sunFAQ/serial/ HTTP/1.0\" 200 0 \"-\" \"nph-churl\"",
    "ip19 - - [24/Apr/2011:05:14:36 -0400] \"GET /vanagon/VanagonProTraining/DigiFant/016.jpg HTTP/1.1\" 200 109813 \"http://www.thesamba.com/vw/forum/viewtopic.php?t=387697&highlight=flat\" \"Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_8; en-us) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4\""
  )

  test("BytePerrequestTransformationTest") {
    val expected = List(
      "ip19,109813.0,109813",
      "ip1,40028.0,40028",
      "ip1565,11586.0,11586",
      "ip1566,256.0,512",
      "ip46,0.0,0"
    )

    val ei_accum = sc.accumulator(0, "EI")
    val ff_accum = sc.accumulator(0, "FIREFOX")
    val other_accum = sc.accumulator(0, "OTHER")

    val accumMap = Map(
      WebClients.EI -> ei_accum,
      WebClients.FIREFOX -> ff_accum,
      WebClients.OTHER -> other_accum)

    val input = sc.parallelize(INPUT_LIST)
    val resList = SparkApp.processFile(input, accumMap).collect().toList
    assert(resList === expected)
    assert(ei_accum.value == 0)
    assert(ff_accum.value == 0)
    assert(other_accum.value == 6)
  }

  test("SkipElementsTest") {
    val source = Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    val filters = Set(1, 2, 3, 4, 5)
    val expected = Array(6, 7, 8, 9)

    val input = sc.parallelize(source)
    val result = SparkApp.skipElements(input, filters).collect()
    assert(result === expected)
  }

  test("GetFirstElements") {
    val source = Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    val expected = Array(1, 2, 3, 4, 5)

    val input = sc.parallelize(source)
    val result = SparkApp.takeFirstElements(input, 5)
    assert(result === expected)
  }
}
