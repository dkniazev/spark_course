package com.epam

/**
  * @author Dmitrii_Kniazev 
  * @since 12/08/2016
  */
object WebClients extends Enumeration {
  val EI, FIREFOX, OTHER = Value
}
