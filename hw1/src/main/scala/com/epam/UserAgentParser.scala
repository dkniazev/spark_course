package com.epam

import net.sf.uadetector.UserAgentFamily._
import net.sf.uadetector.service.UADetectorServiceFactory

/**
  * @author Dmitrii_Kniazev 
  * @since 12/09/2016
  */
object UserAgentParser {

  private val USER_AGENT_PARSER = UADetectorServiceFactory.getResourceModuleParser

  /**
    * Try find web client if possible.
    *
    * @param clientStr sting with client
    * @return { @code IE} or { @code MOZILLA} or { @code OTHER} value
    */
  def checkClientType(clientStr: String): WebClients.Value = {
    val agent = USER_AGENT_PARSER.parse(clientStr)
    agent.getFamily match {
      case IE | IE_MOBILE => WebClients.EI
      case FIREFOX |
           MOBILE_FIREFOX |
           FIREFOX_BONECHO |
           FIREFOX_GRANPARADISO |
           FIREFOX_LORENTZ |
           FIREFOX_MINEFIELD |
           FIREFOX_NAMOROKA |
           FIREFOX_SHIRETOKO => WebClients.FIREFOX
      case _ =>
        WebClients.OTHER
    }
  }
}
