package com.epam

import org.apache.log4j.Logger
import org.apache.spark.rdd.RDD
import org.apache.spark.{Accumulator, SparkConf, SparkContext}

/**
  * @author Dmitrii_Kniazev 
  * @since 12/07/2016
  */
object SparkApp extends App {
  private val LOG: Logger = Logger.getLogger(SparkApp.getClass)

  if (args.length < 2) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: byteperrequest <in> <out>")
    System.exit(2)
  }
  private val inPath = args(0)
  private val outPath = args(1)

  private val appName = "BytePerRequestSparkApp"
  private val masterUrl = "local"
  private val conf = new SparkConf().setAppName(appName)
  //set local mode
  if (args.length == 3 && args(2) == masterUrl) {
    conf.setMaster(masterUrl)
  }

  private val sc = new SparkContext(conf)
  LOG.info("SparkContext created successfully")

  private val ei_accum = sc.accumulator(0, "EI")
  private val ff_accum = sc.accumulator(0, "FIREFOX")
  private val other_accum = sc.accumulator(0, "OTHER")
  private val accumMap = Map(
    WebClients.EI -> ei_accum,
    WebClients.FIREFOX -> ff_accum,
    WebClients.OTHER -> other_accum)

  private val textFile = sc.textFile(inPath)
  private val preparedFile = processFile(textFile, accumMap)
  preparedFile.cache()
  LOG.info("File Prepared")

  val firstFive = takeFirstElements(preparedFile, 5)
  println("First 5 values:")
  firstFive.foreach(println)
  LOG.info("Output console write")


  skipElements(preparedFile, firstFive.toSet).saveAsTextFile(outPath)
  LOG.info("Output file write")

  println("Browser uses stats:")
  println(ei_accum.name.get + ": " + ei_accum)
  println(ff_accum.name.get + ": " + ff_accum)
  println(other_accum.name.get + ": " + other_accum)

  LOG.info("Job done")

  /**
    * Main transformation RDD
    *
    * @param input    text file RDD
    * @param accumMap must contain accumulators for
    * @return ip list sorted by total amount of bytes WebClients values
    */
  def processFile(input: RDD[String],
                  accumMap: Map[WebClients.Value, Accumulator[Int]]): RDD[String] = {
    input.map(str => parseStringAndSetValues(str, accumMap))
      .reduceByKey { case ((size1, num1), (size2, num2)) => (size1 + size2, num1 + num2) }
      .sortBy(tup => tup._2._1, ascending = false)
      .coalesce(1, shuffle = true)
      .map(tup => tup._1 + "," + ((1.0 * tup._2._1) / tup._2._2) + "," + tup._2._1)
  }

  /**
    * Take first elements from RDD as array
    *
    * @param input source RDD
    * @param num   number of elements
    * @return array containing specified number of first elements
    */
  def takeFirstElements[T](input: RDD[T], num: Int): Array[T] = {
    input.take(num)
  }

  /**
    * Remove elements specified at set from RDD
    *
    * @param input source RDD
    * @param set   of elements to remove from RDD
    * @return RDD without elements specified at set
    */
  def skipElements[T](input: RDD[T], set: Set[T]): RDD[T] = {
    input.filter(value => !set.contains(value))
  }

  /**
    * Parse input string and set key and value, return used web client<br/>
    *
    * @param inputStr input string
    * @param accumMap must contain accumulators for
    * @return type of web client { @link WebClientsCounters}
    */
  private def parseStringAndSetValues(inputStr: String,
                                      accumMap: Map[WebClients.Value, Accumulator[Int]]) = {
    //Input example
    //ip1 - - [24/Apr/2011:04:06:01 -0400] "GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1" 200 40028 "-" "Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)"
    //ip46 - - [27/Apr/2011:22:24:05 -0400] "GET /sunFAQ/faq_framebuffer/framebuffer.html HTTP/1.1" 304 - "-" "Sogou web spider/4.0(+http://www.sogou.com/docs/help/webmasters.htm#07)"
    //ip1565 - - [27/Apr/2011:22:27:27 -0400] "GET /sun_ipx/ HTTP/1.1" 200 11586 "-" "MLBot (www.metadatalabs.com/mlbot)"
    //ip1566 - - [27/Apr/2011:22:28:50 -0400] "HEAD /sunFAQ/serial/ HTTP/1.0" 200 0 "-" "nph-churl"
    //ip19 - - [24/Apr/2011:05:14:36 -0400] "GET /vanagon/VanagonProTraining/DigiFant/016.jpg HTTP/1.1" 200 109813 "http://www.thesamba.com/vw/forum/viewtopic.php?t=387697&highlight=flat" "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_8; en-us) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4"
    val splitTest = inputStr.split(" \"")
    accumMap(UserAgentParser.checkClientType(splitTest(3))) += 1
    (getIp(splitTest(0)), (getByteSize(splitTest(1)), 1))
  }

  /**
    * Find ip in input string<br/>
    * Example: "ip1 - - [24/Apr/2011:04:06:01 -0400]"
    * Return: "ip1"
    *
    * @param ipStr sting with ip
    * @return ip
    */
  private def getIp(ipStr: String): String = ipStr.split(" ", 2)(0)

  /**
    * Find size of transferred bytes in input string<br/>
    * Example: "GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028"
    * Return: "40028"
    * Example: "GET /sunFAQ/faq_framebuffer/framebuffer.html HTTP/1.1" 304 -"
    * Return: "0"
    *
    * @param requestStr string with
    * @return number of byte or 0 if not found
    */
  private def getByteSize(requestStr: String): Int = {
    val byteStr = requestStr.split(" ", 5)(4)
    if (byteStr.matches("\\d+")) {
      byteStr.toInt
    } else {
      0
    }
  }
}
