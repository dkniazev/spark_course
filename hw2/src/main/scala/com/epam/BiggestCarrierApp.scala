package com.epam

import com.epam.shared.SparkUtils
import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SQLContext}

/**
  * @author Dmitrii_Kniazev 
  * @since 12/14/2016
  */
object BiggestCarrierApp extends App {
  private val LOG: Logger = Logger.getLogger(BiggestCarrierApp.getClass)

  if (args.length < 1) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: biggestcarrier <flights>")
    System.exit(2)
  }

  private val inPath = args(0)

  private val appName = "BiggestCarrierSparkApp"
  private val masterUrl = System.getProperty("masterUrl")

  val sc = SparkUtils.getSparkContext(appName, masterUrl)
  val sqlContext = new SQLContext(sc)
  val df = SparkUtils.getFlightsDf(sqlContext, inPath)

  val result = findBiggestCarrier(df)

  if (args.length >= 2) {
    val outPath = args(1)
    SparkUtils.saveAsCsv(result, outPath)
  }

  result.show()

  def findBiggestCarrier(df: DataFrame): DataFrame = {
    df.groupBy("uniquecarrier")
      .agg(count("flightnum").alias("flight_per_carrier"))
      .orderBy(col("flight_per_carrier").desc)
      .limit(1)
      .select("flight_per_carrier", "uniquecarrier")
  }
}
