package com.epam

import com.epam.shared.SparkUtils
import org.apache.log4j.Logger
import org.apache.spark.sql.functions.{col, count, sum}
import org.apache.spark.sql.{DataFrame, SQLContext}

/**
  * @author Dmitrii_Kniazev 
  * @since 12/14/2016
  */
object BusyAirportsApp extends App {
  private val LOG = Logger.getLogger(BusyAirportsApp.getClass)

  if (args.length < 2) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: busyairports <flights> <airports>")
    System.exit(2)
  }
  private val flightsPath = args(0)
  private val airportsPath = args(1)

  private val appName = "BusyAirportsSparkApp"
  private val masterUrl = System.getProperty("masterUrl")

  val sc = SparkUtils.getSparkContext(appName, masterUrl)

  val sqlContext = new SQLContext(sc)
  val flightsDf = SparkUtils.getFlightsDf(sqlContext, flightsPath)
  val airportsDf = SparkUtils.getAirportsDf(sqlContext, airportsPath)

  val resultFull = findAirportsLoadForMonths(flightsDf, airportsDf, 6, 8)
  val result = resultFull.limit(5)

  if (args.length >= 3) {
    val outPath = args(2)
    SparkUtils.saveAsCsv(result, outPath)
  }

  result.show()

  def findAirportsLoadForMonths(flights: DataFrame,
                                airports: DataFrame,
                                from_month: Int,
                                to_month: Int): DataFrame = {
    val fl = flights.as("fl")
    val ap_o = airports.as("ap_o")
      .select(col("iata").as("iata_o"),
        col("city").as("city_o"),
        col("country").as("country_o"))
    val ap_d = airports.as("ap_d")
      .select(col("iata").as("iata_d"),
        col("city").as("city_d"),
        col("country").as("country_d"))

    val originDf = fl.join(ap_o, fl("origin") === ap_o("iata_o"), "full")
      .where(fl("month") >= from_month
        and fl("month") <= to_month
        and ap_o("country_o") === "USA")
      .groupBy(fl("origin"))
      .agg(count(fl("flightnum")) as "sum_part")
      .select(fl("origin").as("ap"), col("sum_part"))

    val destDf = fl.join(ap_d, fl("dest") === ap_d("iata_d"), "full")
      .where(fl("month") >= from_month
        and fl("month") <= to_month
        and ap_d("country_d") === "USA")
      .groupBy(fl("dest"))
      .agg(count(fl("flightnum")) as "sum_part")
      .select(fl("dest").as("ap"), col("sum_part"))

    val unitedDf = originDf.unionAll(destDf)

    unitedDf.groupBy("ap")
      .agg(sum("sum_part").as("sum_total"))
      .orderBy(col("sum_total").desc)
      .select(col("sum_total"), col("ap"))
  }
}
