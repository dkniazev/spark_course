package com.epam

import com.epam.shared.SparkUtils
import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions.count

/**
  * @author Dmitrii_Kniazev
  * @since 12/12/2016
  */
object FlightPerCarrierApp extends App {
  private val LOG: Logger = Logger.getLogger(FlightPerCarrierApp.getClass)

  if (args.length < 1) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: flightpercarrier <flights>")
    System.exit(2)
  }

  private val inPath = args(0)

  private val appName = "FlightPerCarrierSparkApp"
  private val masterUrl = System.getProperty("masterUrl")

  val sc = SparkUtils.getSparkContext(appName, masterUrl)

  val sqlContext = new SQLContext(sc)
  val df = SparkUtils.getFlightsDf(sqlContext, inPath)

  val result = findNumberOfFlightsPerCarrier(df)

  if (args.length >= 2) {
    val outPath = args(1)
    SparkUtils.saveAsCsv(result, outPath)
  }

  result.show()

  def findNumberOfFlightsPerCarrier(df: DataFrame): DataFrame = {
    df.groupBy("uniquecarrier")
      .agg(count("flightnum").alias("flight_per_carrier"))
      .select("flight_per_carrier", "uniquecarrier")
  }
}
