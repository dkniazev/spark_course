package com.epam.example

import com.epam.shared.SparkUtils
import org.apache.log4j.Logger

/**
  * @author Dmitrii_Kniazev 
  * @since 12/12/2016
  */
object FileSamplerApp extends App {
  private val LOG: Logger = Logger.getLogger(FileSamplerApp.getClass)

  if (args.length < 1) {
    LOG.error("Current number of parameters less than 1")
    System.exit(2)
  }

  private val appName = "FileSamplerSparkApp"
  private val masterUrl = System.getProperty("masterUrl")
  val sc = SparkUtils.getSparkContext(appName, masterUrl)
  val sqlContext = new org.apache.spark.sql.SQLContext(sc)

  for (file <- args) {
    if (file.endsWith(".csv")) {

      val df = sqlContext.read
        .format("com.databricks.spark.csv")
        .option("header", "true") // Use first line of all files as header
        .option("inferSchema", "true") // Automatically infer data types
        .load(file)
      println("File Sample:" + file)
      df.show()
    } else {
      LOG.error("This is not CSV file:" + file)
    }
  }
}
