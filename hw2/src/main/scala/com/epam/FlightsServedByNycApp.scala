package com.epam

import com.epam.shared.SparkUtils
import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions._

/**
  * @author Dmitrii_Kniazev
  * @since 12/12/2016
  */
object FlightsServedByNycApp extends App {
  private val LOG = Logger.getLogger(FlightsServedByNycApp.getClass)

  if (args.length < 2) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: flightsservedbynyc <flights> <airports>")
    System.exit(2)
  }
  private val flightsPath = args(0)
  private val airportsPath = args(1)

  private val appName = "FlightsServedByNycSparkApp"
  private val masterUrl = System.getProperty("masterUrl")

  val sc = SparkUtils.getSparkContext(appName, masterUrl)

  val sqlContext = new SQLContext(sc)
  val flightsDf = SparkUtils.getFlightsDf(sqlContext, flightsPath)
  val airportsDf = SparkUtils.getAirportsDf(sqlContext, airportsPath)

  val result = findNumberOfFlightsServedByNyc(flightsDf, airportsDf)

  if (args.length >= 3) {
    val outPath = args(2)
    SparkUtils.saveAsCsv(result, outPath)
  }

  result.show()

  def findNumberOfFlightsServedByNyc(flights: DataFrame, airports: DataFrame): DataFrame = {
    val fl = flights.as("fl")
    val ap_o = airports.as("ap_o")
      .select(col("iata").as("iata_o"), col("city").as("city_o"))
    val ap_d = airports.as("ap_d")
      .select(col("iata").as("iata_d"), col("city").as("city_d"))

    fl.join(ap_o, fl("origin") === ap_o("iata_o"), "left")
      .join(ap_d, fl("dest") === ap_d("iata_d"), "left")
      .where(fl("month") === 6
        and (ap_o("city_o") === "New York" or ap_d("city_d") === "New York"))
      .select(count("flightnum") as "flight_per_carrier")
  }
}
