package com.epam.shared

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.types._
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Dmitrii_Kniazev
  * @since 12/12/2016
  */
object SparkUtils {
  private val LOG: Logger = Logger.getLogger(SparkUtils.getClass)

  def getSparkContext(appName: String, masterUrl: String): SparkContext = {
    val conf = new SparkConf().setAppName(appName)
    LOG.info("SparkConfig created successfully")
    //set local mode
    if (masterUrl != null && !masterUrl.eq("")) {
      conf.setMaster(masterUrl)
      LOG.info("Mater URL set as: \"" + masterUrl + "\"")
    }

    val sc = new SparkContext(conf)
    LOG.info("SparkContext created successfully")
    sc
  }

  def getFlightsDf(sqlContext: SQLContext, path: String): DataFrame = {
    sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .schema(SparkUtils.getFlightsSchema)
      .load(path)
  }

  def getAirportsDf(sqlContext: SQLContext, path: String): DataFrame = {
    sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .schema(SparkUtils.getAirportsScheme)
      .load(path)
  }

  def getCarriersDf(sqlContext: SQLContext, path: String): DataFrame = {
    sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .schema(SparkUtils.getCarriersScheme)
      .load(path)
  }

  def getFlightsSchema: StructType = {
    StructType(Array(
      StructField("year", StringType), //1
      StructField("month",  StringType), //2
      StructField("dayofmonth", StringType), //3
      StructField("dayofweek", StringType), //4
      StructField("deptime", StringType), //5
      StructField("crsdeptime", StringType), //6
      StructField("arrtime", StringType), //7
      StructField("crsarrtime", StringType), //8
      StructField("uniquecarrier", StringType), //9
      StructField("flightnum", StringType), //10
      StructField("tailnum", StringType), //11
      StructField("actualelapsedtime", StringType), //12
      StructField("crselapsedtime", StringType), //13
      StructField("airtime", StringType), //14
      StructField("arrdelay", StringType), //15
      StructField("depdelay", StringType), //16
      StructField("origin", StringType), //17
      StructField("dest", StringType), //18
      StructField("distance", StringType), //19
      StructField("taxiin", StringType), //20
      StructField("taxiout", StringType), //21
      StructField("cancelled", StringType), //22
      StructField("cancellationcode", StringType, nullable = false), //23
      StructField("diverted", StringType), //24
      StructField("carrierdelay", StringType), //25
      StructField("weatherdelay", StringType), //26
      StructField("nasdelay", StringType), //27
      StructField("securitydelay", StringType), //28
      StructField("lateaircraftdelay", StringType))) //29
  }

  def getCarriersScheme: StructType = {
    StructType(Array(
      StructField("code", StringType),
      StructField("description", StringType)))
  }

  def getAirportsScheme: StructType = {
    StructType(Array(
      StructField("iata", StringType),
      StructField("airport", StringType),
      StructField("city", StringType),
      StructField("state", StringType),
      StructField("country", StringType),
      StructField("lat", StringType),
      StructField("long", StringType)))
  }

  def saveAsCsv(df: DataFrame, outPath: String): Unit = {
    df.coalesce(1)
      .write
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .save(outPath)
  }
}
