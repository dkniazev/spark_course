package com.epam

import com.epam.shared.SparkUtils
import com.holdenkarau.spark.testing.SharedSparkContext
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * @author Dmitrii_Kniazev
  * @since 12/07/2016
  */
@RunWith(classOf[JUnitRunner])
class FlightHandlerSuite extends FunSuite with SharedSparkContext {
  val FLIGHTS_LIST = Array(
    "2007,6,1,1,1232,1225,1341,1340,WN,2891,N351,69,75,54,1,7,SMB,SMA,389,4,11,0,,0,0,0,0,0,0",
    "2007,2,1,1,1918,1905,2043,2035,AB,462,N370,85,90,74,8,13,SMY,SMY,479,5,6,0,,0,0,0,0,0,0",
    "2007,3,1,1,2206,2130,2334,2300,WN,1229,N685,88,90,73,34,36,SMY,SMC,479,6,9,0,,0,3,0,0,0,31",
    "2007,6,1,1,1230,1200,1356,1330,WN,1355,N364,86,90,75,26,30,SMY,SMC,479,3,8,0,,0,23,0,0,0,3",
    "2007,5,1,1,831,830,957,1000,AB,2278,N480,86,90,74,-3,1,SMC,SMY,479,3,9,0,,0,0,0,0,0,0",
    "2007,6,1,1,1430,1420,1553,1550,CN,2386,N611SW,83,90,74,3,10,SMA,SMY,479,2,7,0,,0,0,0,0,0,0"
  )

  val AIRPORTS_LIST = Array(
    "SMA,Lewis County Regional,Monticello,MO,USA,40.12916667,-91.67833333",
    "SMB,Lee County-Marianna,Marianna,AR,USA,34.78027778,-90.81055556",
    "SMC,Marked Tree Municipal,Marked Tree,AR,USA,35.53341472,-90.40149028",
    "SMY,E 34th St Heliport,New York,NY,USA,40.74260167,-73.97208306"
  )

  test("FlightPerCarrierAppTest") {
    val sqlContext = new SQLContext(sc)
    val df = getFlightsDf(sqlContext)
    val result = FlightPerCarrierApp
      .findNumberOfFlightsPerCarrier(df)
      .sort("uniquecarrier")
      .map(row => (row.get(0), row.get(1)))
      .collect()

    val expected = Array((2, "AB"), (1, "CN"), (3, "WN"))
    assert(result === expected)
  }

  test("FlightsServedByNyc") {
    val sqlContext = new SQLContext(sc)
    val flightDf = getFlightsDf(sqlContext)
    val airportDf = getAirportsDf(sqlContext)

    val result = FlightsServedByNycApp
      .findNumberOfFlightsServedByNyc(flightDf, airportDf)
      .collect().head.get(0)

    val expected = 2L
    assert(result === expected)
  }

  test("MostBusyAirports") {
    val sqlContext = new SQLContext(sc)
    val flightDf = getFlightsDf(sqlContext)
    val airportDf = getAirportsDf(sqlContext)

    var result = BusyAirportsApp
      .findAirportsLoadForMonths(flightDf, airportDf, 2, 5)
      .map(row => (row.get(0), row.get(1)))
      .collect()

    var expected = Array((4, "SMY"), (2, "SMC"))
    assert(result === expected)

    result = BusyAirportsApp
      .findAirportsLoadForMonths(flightDf, airportDf, 3, 6)
      .map(row => (row.get(0), row.get(1)))
      .collect()

    expected = Array((4, "SMY"), (3, "SMC"), (2, "SMA"), (1, "SMB"))
    assert(result === expected)
  }

  test("BiggestCarrier") {
    val sqlContext = new SQLContext(sc)
    val df = getFlightsDf(sqlContext)
    val result = BiggestCarrierApp.findBiggestCarrier(df)
      .map(row => (row.get(0), row.get(1)))
      .collect()

    val expected = Array((3, "WN"))
    assert(result === expected)
  }

  private def getFlightsDf(sqlContext: SQLContext): DataFrame = {
    val rowRdd = sc.parallelize(FLIGHTS_LIST)
      .map(s => Row.fromSeq(s.split(",")))
    sqlContext.createDataFrame(rowRdd, SparkUtils.getFlightsSchema)
  }


  private def getAirportsDf(sqlContext: SQLContext): DataFrame = {
    val rowRdd = sc.parallelize(AIRPORTS_LIST)
      .map(s => Row.fromSeq(s.split(",")))
    sqlContext.createDataFrame(rowRdd, SparkUtils.getAirportsScheme)
  }
}
