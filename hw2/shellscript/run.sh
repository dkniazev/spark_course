#!/bin/bash bash

#1 get samples from files
spark-submit --class com.epam.example.FileSamplerApp flighthandler-0.1-SNAPSHOT.jar hdfs://sandbox.hortonworks.com:8020/hwh1/2007.csv hdfs://sandbox.hortonworks.com:8020/hwh1/carriers.csv hdfs://sandbox.hortonworks.com:8020/hwh1/airports.csv
#2 number of flights per carrier
spark-submit --class com.epam.FlightPerCarrierApp flighthandler-0.1-SNAPSHOT.jar hdfs://sandbox.hortonworks.com:8020/hwh1/2007.csv hdfs://sandbox.hortonworks.com:8020/out2s/
#3 total number of flights served in Jun 2007 by NYC
spark-submit --class com.epam.FlightsServedByNycApp flighthandler-0.1-SNAPSHOT.jar hdfs://sandbox.hortonworks.com:8020/hwh1/2007.csv hdfs://sandbox.hortonworks.com:8020/hwh1/airports.csv hdfs://sandbox.hortonworks.com:8020/out2s/
#4 most busy airports
spark-submit --class com.epam.BusyAirportsApp flighthandler-0.1-SNAPSHOT.jar hdfs://sandbox.hortonworks.com:8020/hwh1/2007.csv hdfs://sandbox.hortonworks.com:8020/hwh1/airports.csv hdfs://sandbox.hortonworks.com:8020/out2s/
#5 carrier served biggest number of flight
spark-submit --class com.epam.BiggestCarrierApp flighthandler-0.1-SNAPSHOT.jar hdfs://sandbox.hortonworks.com:8020/hwh1/2007.csv hdfs://sandbox.hortonworks.com:8020/out2s/
