#!/bin/bash bash
bin/zookeeper-server-start.sh config/zookeeper.properties
bin/kafka-server-start.sh config/server.properties
#create topic
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic alerts
#show topics list
#bin/kafka-topics.sh --list --zookeeper localhost:2181
#consumer
#bin/kafka-console-consumer.sh --bootstrap-server --zookeeper localhost:2181 localhost:9092 --topic alerts --from-beginning
