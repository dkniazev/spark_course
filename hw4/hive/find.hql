CREATE TEMPORARY TABLE IF NOT EXISTS statistics_by_hour_2 (
    hostip STRING,
    trafficconsumed DOUBLE,
    averagespeed DOUBLE,
    time TIMESTAMP)
COMMENT 'IMP TEMPORARY TABLE'
ROW FORMAT DELIMITED;

INSERT OVERWRITE TABLE statistics_by_hour_2 SELECT * FROM statistics_by_hour;

--faster ip by time
SELECT t.hostip, AVG(t.averagespeed) as avg_sum FROM statistics_by_hour_2 as t
WHERE t.time >= unix_timestamp("2016-12-27 13:29:34.0")
GROUP BY t.hostip
ORDER BY avg_sum DESC
LIMIT 3;
--faster ip at all time
SELECT t.hostip, AVG(t.averagespeed) as avg_sum FROM statistics_by_hour_2 as t
GROUP BY t.hostip
ORDER BY avg_sum DESC
LIMIT 3;

