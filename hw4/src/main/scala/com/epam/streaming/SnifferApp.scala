package com.epam.streaming

import com.epam.streaming.util.{IpProperty, KafkaMessage, Settings}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SaveMode}
import org.apache.spark.streaming._
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.{Logging, SparkConf, SparkContext}

/**
  * @author Dmitrii_Kniazev
  * @since 12/14/2016
  */
object SnifferApp extends App with Logging {

  private val appName = "SnifferSparkApp"
  private val masterUrl = System.getProperty("masterUrl")
  private val localSettings = System.getProperty("localSettings")
  private val notSendToKafka = isNotSendToKafka
  private val notAggregateStats = isNotAggregateStats
  private val batchDuration = Seconds(1)
  private val statsDuration = Minutes(60)

  private val outputTableName = "statistics_by_hour"
  val kafkaBrokers = "127.0.0.1:9092"
  val kafkaTopic = "alerts"

  val conf = new SparkConf().setAppName(appName)
  logInfo("SparkConfig created successfully")

  if (masterUrl != null && !masterUrl.eq("")) {
    conf.setMaster(masterUrl)
    logInfo("Mater URL set as: \"" + masterUrl + "\"")
  }
  val sc = new SparkContext(conf)
  //Load settings
  val settings = new Settings(sc, localSettings)
  val settingsRdd = settings.getRdd
  val windowsDuration = settings.getWindowsDuration

  logInfo("Settings loaded successfully")

  //Set address for sniffing
  private val hostName = if (args.length >= 1) args(0) else "127.0.0.1"
  private val checkpointDir = "/checkpoints"

  val ssc = StreamingContext.getOrCreate(checkpointDir, createStreamingContext)
  logInfo("SparkStreamingContext created successfully")

  //Create stream {key - ip; value - packet size (bytes)}
  val snifferStream = ssc.receiverStream(new PacketReceiver(hostName))
  val aggregated = snifferStream.reduceByKey(_ + _)

  if (!notSendToKafka) {
    val stateSpec = StateSpec.function(trackStateFunc _)
    for (duration <- windowsDuration) {
      val filteredSettings = settingsRdd.filter(tup => tup._2.period == duration).cache()
      createWindowFunc(aggregated, filteredSettings, Seconds(duration), stateSpec)
    }
  }

  if (!notAggregateStats) {
    val hiveContext = new HiveContext(sc)
    //Window for collect statistics and write to Hive
    val byHourStats = aggregated.reduceByKeyAndWindow(_ + _, statsDuration, slideDuration = statsDuration)
    byHourStats.foreachRDD((rdd, time) => {
      val stats = rdd.map({ case (ip, size) => createStatRow(ip, size, time) })
        .sortBy(_.getDouble(1), ascending = false)
      val statsTable = hiveContext.createDataFrame(stats, getOutputSchema)
      statsTable.write.mode(SaveMode.Append).saveAsTable(outputTableName)
    })
  }

  //start streaming
  ssc.start()
  ssc.awaitTermination()

  //Driver Fault Tolerance
  private def createStreamingContext(): StreamingContext = {
    val ssc = new StreamingContext(sc, batchDuration)
    ssc.checkpoint(checkpointDir)
    ssc
  }

  private def createWindowFunc(stream: DStream[(String, Int)],
                               settings: RDD[(String, IpProperty)],
                               duration: Duration,
                               stateSpec: StateSpec[String, (Int, IpProperty), (Boolean, Boolean), KafkaMessage]
                              ): Unit = {
    val window = stream.reduceByKeyAndWindow(_ + _, duration, slideDuration = batchDuration)
    val joinedWindow = window.transform(rdd => rdd.join(settings))
    val mapped = joinedWindow.mapWithState(stateSpec)
    mapped.foreachRDD(rdd => {
      rdd.collect().foreach(println)
      rdd.foreachPartition(partition => {
        val props = new java.util.HashMap[String, AnyRef]()
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers)
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
          "org.apache.kafka.common.serialization.StringSerializer")
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
          "org.apache.kafka.common.serialization.StringSerializer")
        //open connection
        val producer = new KafkaProducer[String, String](props)
        partition.foreach(record => {
          val message = new ProducerRecord[String, String](kafkaTopic, record.uuid, record.toJson)
          producer.send(message)
        })
        //close connection
        producer.close()
      })
    })
  }


  private def createStatRow(ip: String, size: Int, time: Time) = {
    val timestamp = getJavaTimestamp(time)
    //size in Mb
    val sizeInMb = byteSizeToMb(size)
    //average speed Mb/sec
    val avgSpeed = sizeInMb / (statsDuration.milliseconds / 1000)
    Row(ip, sizeInMb, avgSpeed, timestamp)
  }

  private def trackStateFunc(batchTime: Time,
                             key: String,
                             value: Option[(Int, IpProperty)],
                             state: State[(Boolean, Boolean)]
                            ): Option[KafkaMessage] = {
    if (value.isDefined) {
      val timestamp = getJavaTimestamp(batchTime)
      val sizeInMb = byteSizeToMb(value.get._1)
      val properties = value.get._2
      val stateVal = getStateByType(state, properties.typeValue)

      if (sizeInMb > properties.value && !stateVal) {
        state.update(getNewStateByType(state, properties.typeValue))
        return Some(new KafkaMessage(timestamp, key, sizeInMb, properties))
      } else if (sizeInMb < properties.value && stateVal) {
        state.update(getNewStateByType(state, properties.typeValue))
      }
    }
    None
  }

  private def getStateByType(state: State[(Boolean, Boolean)],
                             typeVal: Int): Boolean = {
    if (state.exists()) {
      typeVal match {
        case 1 => state.get()._1
        case 2 => state.get()._2
        case _ => throw new IllegalArgumentException("Not valid type:" + typeVal)
      }
    } else {
      state.update((false, false))
      false
    }
  }

  private def getNewStateByType(state: State[(Boolean, Boolean)],
                                typeVal: Int): (Boolean, Boolean) = {
    typeVal match {
      case 1 => (!state.get()._1, state.get()._2)
      case 2 => (state.get()._1, !state.get()._2)
      case _ => throw new IllegalArgumentException("Not valid type:" + typeVal)
    }
  }

  private def byteSizeToMb(sizeInByte: Int): Double = {
    (sizeInByte * 1.0) / 1048576
  }

  private def getJavaTimestamp(time: Time) = {
    new java.sql.Timestamp(time.milliseconds)
  }

  private def getOutputSchema: StructType = {
    StructType(Array(
      StructField("hostIp", StringType),
      StructField("trafficConsumed", DoubleType),
      StructField("averageSpeed", DoubleType),
      StructField("timestamp", TimestampType)
    ))
  }

  private def isNotSendToKafka: Boolean = {
    val property = System.getProperty("notSendToKafka")
    if (property != null && property == "true") true else false
  }

  private def isNotAggregateStats: Boolean = {
    val property = System.getProperty("notAggregateStats")
    if (property != null && property == "true") true else false
  }
}
