package com.epam.streaming

import java.net.InetAddress

import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode
import org.pcap4j.core.Pcaps
import org.pcap4j.packet.IpV4Packet
import org.pcap4j.packet.namednumber.IpNumber

/**
  * @author Dmitrii_Kniazev 
  * @since 12/19/2016
  */
object PcapTest extends App {
  val addr = InetAddress.getByName("10.16.11.29")
  val nif = Pcaps.getDevByAddress(addr)

  val snapLen = 65536
  val mode = PromiscuousMode.PROMISCUOUS
  val timeout = 1000
  val handle = nif.openLive(snapLen, mode, timeout)

  val mask = "10.16.11.29"
  while (true) {
    val packet = handle.getNextPacket
    if (packet != null) {
      val ipV4Packet: IpV4Packet = packet.get(classOf[IpV4Packet])
      if (ipV4Packet != null && ipV4Packet.getHeader.getProtocol == IpNumber.TCP) {
        val raw = ipV4Packet.getRawData.slice(2, 4)
        //val value += ( raw[0].toLong & 0xffL) << (8 * 1)
        val packetLength = ipV4Packet.length()
        val srcAdr = ipV4Packet.getHeader.getSrcAddr.getHostAddress
        val dstAdr = ipV4Packet.getHeader.getDstAddr.getHostAddress

        if (srcAdr == mask || dstAdr == mask) {
          println(srcAdr + " > " + dstAdr + " : L > " + packetLength)
        }

      }
    }
  }
  handle.close()
}
