package com.epam.streaming.util

import java.sql.Timestamp

import kafka.utils.Json


/**
  * @author Dmitrii_Kniazev 
  * @since 12/26/2016
  */
final case class KafkaMessage private(uuid: String,
                                      private val timestamp: Timestamp,
                                      private val hostIp: String,
                                      private val `type`: Int,
                                      private val factValue: Double,
                                      private val thresholdValue: Double,
                                      private val period: Long) {
  private val map = Map(
    "ID" -> uuid,
    "Timestamp" -> timestamp.toString,
    "HostIP" -> hostIp,
    "Type" -> `type`,
    "FactValue" -> factValue,
    "ThresholdValue" -> thresholdValue,
    "Period" -> period
  )

  def this(timestamp: Timestamp,
           hostIp: String,
           factValue: Double,
           ipProperty: IpProperty) {
    this(java.util.UUID.randomUUID().toString,
      timestamp,
      hostIp,
      ipProperty.typeValue,
      factValue,
      ipProperty.value,
      ipProperty.period)
  }

  def toJson: String = {
    Json.encode(map)
  }

}
