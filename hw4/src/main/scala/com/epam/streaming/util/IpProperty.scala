package com.epam.streaming.util

/**
  * @author Dmitrii_Kniazev 
  * @since 12/23/2016
  */
final case class IpProperty(typeValue: Int, value: Double, period: Long)
