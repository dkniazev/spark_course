package com.epam.streaming.util

import com.epam.streaming.util.Settings._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions.{coalesce, col}
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.apache.spark.{Logging, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * @author Dmitrii_Kniazev 
  * @since 12/20/2016
  */
final class Settings(val sc: SparkContext, val path: String) extends Logging {


  private val allSettings = if (path == null || path == "") {
    getSettingsFromHive(sc).cache()
  } else {
    getSettingsFromLocalFile(sc, path).cache()
  }
  //Check existing only 2 default rows
  if (!isValidDefaultSettingsNum(allSettings)) {
    val err = "Default settings number not equal 2"
    logError(err)
    throw new IllegalStateException(err)
  }
  //Validate format default settings
  if (!isValidDefaultSettingsFormat(allSettings)) {
    val err = "Default settings has invalid format"
    logError(err)
    throw new IllegalStateException(err)
  }

  //find only valid settings
  private val srcSettings = allSettings.where((ipCol !== "NULL") and (ipCol !== ""))
    .where(typeCol === 1 or typeCol === 2)
    .where(valueCol.isNull or valueCol > 0)
    .where(periodCol.isNull or periodCol > 0)
    .select(ipCol, typeCol, valueCol, periodCol)

  if (srcSettings.groupBy(ipCol, typeCol).count()
    .where(col("count") > 1)
    .count() != 0) {
    val err = "Settings contain more then 1 rows with same ip and type"
    logError(err)
    throw new IllegalStateException(err)
  }

  private val defaultSettings = allSettings.where(ipCol === "NULL")
  private val joinedSettings = prepareSettings(defaultSettings, srcSettings)
  private val windowArray = getWindowArray(joinedSettings)

  def getWindowsDuration: Array[Long] = windowArray

  def getRdd: RDD[(String, IpProperty)] = joinedSettings.map(settingsRow2Pair)

  private def getSettingsFromHive(sc: SparkContext): DataFrame = {
    // sc is an existing SparkContext.
    val hiveContext = new HiveContext(sc)
    //Check what table exist
    if (hiveContext.tableNames().count(_.equals("settings")) != 1) {
      val err = "Table 'settings' not found"
      logError(err)
      throw new IllegalStateException(err)
    }
    hiveContext.sql("SELECT host_ip, type, value, period FROM settings")
  }

  private def getSettingsFromLocalFile(sc: SparkContext, path: String): DataFrame = {
    val sqlContext = new SQLContext(sc)
    val settings = sqlContext
      .read
      .format("com.databricks.spark.csv")
      .option("header", "false")
      .schema(schema).load(path)
    settings
  }

  private def prepareSettings(defaultSettings: DataFrame, sourceSettings: DataFrame): DataFrame = {
    val ss = sourceSettings.as("ss")
    val ds = defaultSettings.as("ds")
      .select(typeCol.as("td"), valueCol.as("vd"), periodCol.as("pd"))

    val res = ss.join(ds, ss(typeName) === ds("td"), "inner")
      .select(ss(ipName),
        ss(typeName),
        coalesce(ss(valueName), ds("vd")).as(valueName),
        coalesce(ss(periodName), ds("pd")).as(periodName)
      )
    res
  }

  private def getWindowArray(settings: DataFrame): Array[Long] = {
    val rows = joinedSettings.groupBy(periodCol.as("tmp")).agg(periodCol).select(periodCol).collect()
    val buf = ArrayBuffer.empty[Long]
    for (row <- rows) {
      buf += row.getLong(0)
    }
    buf.toArray
  }

}

object Settings {
  //Table columns name
  private val ipName = "host_ip"
  private val typeName = "type"
  private val valueName = "value"
  private val periodName = "period"
  //Table columns
  private val ipCol = col(ipName)
  private val typeCol = col(typeName)
  private val valueCol = col(valueName)
  private val periodCol = col(periodName)

  val schema = StructType(Array(
    StructField(ipName, StringType),
    StructField(typeName, IntegerType),
    StructField(valueName, DoubleType),
    StructField(periodName, LongType)
  ))

  def isValidDefaultSettingsNum(settingsDf: DataFrame): Boolean = {
    settingsDf.where(ipCol === "NULL").count() == 2
  }

  def isValidDefaultSettingsFormat(settingsDf: DataFrame): Boolean = {
    settingsDf.where(ipCol === "NULL")
      .where(typeCol === 1 or typeCol === 2)
      .where(valueCol.isNotNull and valueCol > 0)
      .where(periodCol.isNotNull and periodCol > 0)
      .groupBy(typeCol).count()
      .count() == 2
  }

  private def settingsRow2Pair(row: Row) = {
    val ip = row.getString(0)
    val typeVal = row.getInt(1)
    val value = row.getDouble(2)
    val period = row.getLong(3)
    (ip, IpProperty(typeVal, value, period))
  }
}