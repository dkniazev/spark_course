package com.epam.streaming

import java.net.InetAddress
import java.util.concurrent.TimeoutException

import org.apache.spark.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode
import org.pcap4j.core.Pcaps
import org.pcap4j.packet.IpV4Packet
import org.pcap4j.packet.namednumber.IpNumber

/**
  * @author Dmitrii_Kniazev 
  * @since 12/20/2016
  */
final class PacketReceiver(private val hostName: String)
  extends Receiver[(String, Int)](StorageLevel.MEMORY_AND_DISK_2) with Logging {

  private val mask = hostName

  override def onStart(): Unit = {
    // Start the thread that receives data over a connection
    new Thread("Socket Receiver") {
      override def run() {
        receive(hostName)
      }
    }.start()
    logInfo("Socket Receiver Started")
  }

  override def onStop(): Unit = {
    // There is nothing much to do as the thread calling receive()
    // is designed to stop by itself if isStopped() returns false
  }

  private def receive(host: String): Unit = {
    val addr = InetAddress.getByName(host)
    val nif = Pcaps.getDevByAddress(addr)

    val snapLen = 65536
    val mode = PromiscuousMode.PROMISCUOUS
    val timeout = 1000
    val handle = nif.openLive(snapLen, mode, timeout)
    while (!isStopped) {
      try {
        val packet = handle.getNextPacketEx
        val ipV4Packet: IpV4Packet = packet.get(classOf[IpV4Packet])
        if (ipV4Packet != null && ipV4Packet.getHeader.getProtocol == IpNumber.TCP) {
          val packetLength = ipV4Packet.length()
          val srcAdr = ipV4Packet.getHeader.getSrcAddr.getHostAddress
          val dstAdr = ipV4Packet.getHeader.getDstAddr.getHostAddress
          if (srcAdr == mask) {
            store((dstAdr, packetLength))
          } else if (dstAdr == mask) {
            store((srcAdr, packetLength))
          }
        }
      } catch {
        case _: TimeoutException => logInfo("Timeout Exception Occurred")
      }
    }
    handle.close()
  }
}
