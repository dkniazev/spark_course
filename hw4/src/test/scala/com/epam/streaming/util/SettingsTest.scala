package com.epam.streaming.util

import java.util

import com.holdenkarau.spark.testing.SharedSparkContext
import org.apache.spark.sql.{Row, SQLContext}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * @author Dmitrii_Kniazev 
  * @since 12/22/2016
  */
@RunWith(classOf[JUnitRunner])
class SettingsTest extends FunSuite with SharedSparkContext {
  private val defaultValidThreshold = Row.fromSeq(Array("NULL", 1, 0.1, 10L))
  private val defaultValidLimit = Row.fromSeq(Array("NULL", 2, 0.3, 5L))

  private val defaultInvalidThreshold1 = Row.fromSeq(Array("NULL", 1, -0.3, 5L))
  private val defaultInvalidThreshold2 = Row.fromSeq(Array("NULL", 1, 0.3, -5L))
  private val defaultInvalidThreshold3 = Row.fromSeq(Array("NULL", 1, 0.3, null))
  private val defaultInvalidThreshold4 = Row.fromSeq(Array("NULL", 1, null, -5L))

  private val defaultInvalidThreshold = Array(
    defaultInvalidThreshold1,
    defaultInvalidThreshold2,
    defaultInvalidThreshold3,
    defaultInvalidThreshold4
  )

  private val defaultInvalidLimit1 = Row.fromSeq(Array("NULL", 2, -0.1, 10L))
  private val defaultInvalidLimit2 = Row.fromSeq(Array("NULL", 2, 0.1, -10L))
  private val defaultInvalidLimit3 = Row.fromSeq(Array("NULL", 2, null, 10L))
  private val defaultInvalidLimit4 = Row.fromSeq(Array("NULL", 2, 0.1, null))

  private val defaultInvalidLimit = Array(
    defaultInvalidLimit1,
    defaultInvalidLimit2,
    defaultInvalidLimit3,
    defaultInvalidLimit4
  )
  test("Check number of default settings") {
    val sqlContext = new SQLContext(sc)
    val rowList = new util.ArrayList[Row](0)
    //Empty list invalid
    val emptyDf = sqlContext.createDataFrame(rowList, Settings.schema)
    assert(!Settings.isValidDefaultSettingsNum(emptyDf))

    //1 settings invalid
    rowList.add(defaultInvalidThreshold1)
    val oneRowDf = sqlContext.createDataFrame(rowList, Settings.schema)
    assert(!Settings.isValidDefaultSettingsNum(oneRowDf))

    //2 settings valid
    rowList.add(defaultInvalidThreshold1)
    val twoRowDf = sqlContext.createDataFrame(rowList, Settings.schema)
    assert(Settings.isValidDefaultSettingsNum(twoRowDf))

    //more than 2 settings invalid
    rowList.add(defaultInvalidThreshold1)
    val treeRowDf = sqlContext.createDataFrame(rowList, Settings.schema)
    assert(!Settings.isValidDefaultSettingsNum(treeRowDf))
  }

  test("Check format of default settings") {
    val sqlContext = new SQLContext(sc)
    //Valid rows
    val validRowList = new util.ArrayList[Row](0) {
      {
        add(defaultValidThreshold)
        add(defaultValidLimit)
      }
    }
    val validFormatDf = sqlContext.createDataFrame(validRowList, Settings.schema)
    assert(Settings.isValidDefaultSettingsFormat(validFormatDf))

    val invalidRowsList = new util.ArrayList[Row](2)
    //Invalid rows
    invalidRowsList.clear()
    invalidRowsList.add(defaultValidLimit)
    invalidRowsList.add(defaultValidLimit)
    assert(!Settings.isValidDefaultSettingsFormat(
      sqlContext.createDataFrame(invalidRowsList, Settings.schema)))

    invalidRowsList.clear()
    invalidRowsList.add(defaultValidThreshold)
    invalidRowsList.add(defaultValidThreshold)
    assert(!Settings.isValidDefaultSettingsFormat(
      sqlContext.createDataFrame(invalidRowsList, Settings.schema)))

    for (r1 <- defaultInvalidThreshold) {
      invalidRowsList.clear()
      invalidRowsList.add(r1)
      invalidRowsList.add(defaultValidThreshold)
      val invalidFormatDf = sqlContext.createDataFrame(invalidRowsList, Settings.schema)
      assert(!Settings.isValidDefaultSettingsFormat(invalidFormatDf))
    }

    for (r1 <- defaultInvalidThreshold; r2 <- defaultInvalidLimit) {
      invalidRowsList.clear()
      invalidRowsList.add(r1)
      invalidRowsList.add(r2)
      val invalidFormatDf = sqlContext.createDataFrame(invalidRowsList, Settings.schema)
      assert(!Settings.isValidDefaultSettingsFormat(invalidFormatDf))
    }
  }
}
