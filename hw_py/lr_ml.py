import sys

import pyspark.mllib.classification as cl
from pyspark.context import SparkContext
from pyspark.mllib.evaluation import BinaryClassificationMetrics
from pyspark.mllib.feature import ChiSqSelector
from pyspark.mllib.feature import StandardScaler
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.regression import LabeledPoint


def line_to_vector(line):
    new_line = []
    split = line.replace(',', '.').split(sep=';')
    for val in split:
        if val.lower() != 'nan':
            new_line.append(float(val))
        else:
            new_line.append(0.0)
    return Vectors.dense(new_line)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('Number of arguments:', len(sys.argv), 'arguments.')
        exit('Usage: testpyapp <datapath>')

    objectsPath = sys.argv[1] + 'Objects.csv'
    targetPath = sys.argv[1] + 'Target.csv'

    sc = SparkContext(master="local", appName="Simple App")

    featuresDs = sc.textFile(objectsPath).map(lambda v: line_to_vector(v)).repartition(1)
    labelsDs = sc.textFile(targetPath).map(float).repartition(1)

    scaledDs = StandardScaler().fit(dataset=featuresDs)
    mainDs = labelsDs.zip(scaledDs.transform(featuresDs))
    mainDs = mainDs.map(lambda t: LabeledPoint(t[0], t[1]))
    selector = ChiSqSelector(30).fit(mainDs).transform(mainDs.map(lambda x: x.features))
    mainDs = labelsDs.zip(selector)
    mainDs = mainDs.map(lambda t: LabeledPoint(t[0], t[1]))
    # split data to training (60%) and test (40%)
    splits = mainDs.randomSplit([0.7, 0.3], 11)
    trainingData = splits[0].cache()
    # print('Training set:', trainingData.count())

    testData = splits[1].cache()
    testNum = testData.count()
    # print('Test set:', testNum)

    # Run Logistic Regression using the SGD algorithm.
    model = cl.LogisticRegressionWithLBFGS.train(trainingData, iterations=100)

    # Compute raw scores on the test set.
    # Evaluating the model on training data
    scoreAndLabels = testData.map(lambda p: (float(model.predict(p.features)), p.label))

    # Get evaluation metrics.
    metrics = BinaryClassificationMetrics(scoreAndLabels)
    auROC = metrics.areaUnderROC
    print("Area under ROC = ", auROC)

    testErr = scoreAndLabels.filter(lambda v: v[0] != v[1]).count() / float(testData.count())
    print('Test Error = ' + str(testErr))
