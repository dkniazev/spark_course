import sys

from pyspark.context import SparkContext
from pyspark.mllib.evaluation import BinaryClassificationMetrics
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.tree import RandomForest


def line_to_vector(line):
    new_line = []
    split = line.replace(',', '.').split(sep=';')
    for val in split:
        if val.lower() != 'nan':
            new_line.append(float(val))
        else:
            new_line.append(0.0)
    return Vectors.dense(new_line)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('Number of arguments:', len(sys.argv), 'arguments.')
        exit('Usage: testpyapp <datapath>')

    objectsPath = sys.argv[1] + 'Objects.csv'
    targetPath = sys.argv[1] + 'Target.csv'

    sc = SparkContext(master="local[*]", appName="Simple App")

    features = sc.textFile(objectsPath).map(lambda v: line_to_vector(v)).repartition(1)
    labels = sc.textFile(targetPath).map(float).repartition(1)
    main_data = labels.zip(features).map(lambda t: LabeledPoint(t[0], t[1]))
    # split data to training (60%) and test (40%)
    splits = main_data.randomSplit([0.7, 0.3], 11)
    trainingData = splits[0].cache()
    # print('Training set:', trainingData.count())

    testData = splits[1].cache()
    testNum = testData.count()
    # print('Test set:', testNum)

    categoricalFeatures = {1: 2,
                           2: 2,
                           3: 2,
                           6: 7,
                           7: 5,
                           8: 31,
                           9: 12,
                           10: 5,
                           11: 2,
                           12: 10,
                           13: 6}
    # Train a RandomForest model.
    #  Empty categoricalFeaturesInfo indicates all features are continuous.
    #  Note: Use larger numTrees in practice.
    #  Setting featureSubsetStrategy="auto" lets the algorithm choose.
    rf_model = RandomForest.trainClassifier(trainingData,
                                            numClasses=2,
                                            categoricalFeaturesInfo=categoricalFeatures,
                                            featureSubsetStrategy="auto",
                                            maxDepth=4,
                                            impurity='variance',
                                            numTrees=3)

    predictions = rf_model.predict(testData.map(lambda x: x.features))
    scoreAndLabels = predictions.zip(testData.map(lambda lp: lp.label))
    # scoreAndLabels = sc.parallelize(
    #     [(0.0, 0.0), (1.0, 1.0), (0.0, 0.0), (0.0, 0.0), (1.0, 1.0), (0.0, 1.0), (1.0, 1.0)],
    # [(0.1, 0.0), (0.1, 1.0), (0.4, 0.0), (0.6, 0.0), (0.6, 1.0), (0.6, 1.0), (0.8, 1.0)],
    # 2)

    # Get evaluation metrics.
    rf_metrics = BinaryClassificationMetrics(scoreAndLabels)
    rf_auROC = rf_metrics.areaUnderROC
    print("RF Area under ROC = ", rf_auROC)

    testErr = scoreAndLabels.filter(lambda v: v[0] != v[1]).count() / float(testData.count())
    print('Test Error = ' + str(testErr))
