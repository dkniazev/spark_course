#!/bin/bash
#1 logistic regression
spark-submit --class com.epam.mlmodels.LogisticRegressionModel mlmodels-0.1-SNAPSHOT.jar hdfs://sandbox.hortonworks.com:8020/hwh3/
#2 random forest
spark-submit --class com.epam.mlmodels.RandomForestModel mlmodels-0.1-SNAPSHOT.jar hdfs://sandbox.hortonworks.com:8020/hwsh/