package com.epam.mlmodels

import org.apache.log4j.Logger
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Dmitrii_Kniazev 
  * @since 12/14/2016
  */
object RandomForestModel extends App {
  private val LOG: Logger = Logger.getLogger(LogisticRegressionModel.getClass)

  //(more info in PropertyDesciptionEN.txt)
  private val isEmployed = 1
  private val isRetired = 2
  private val sex = 3
  private val education = 6
  private val maritalStatus = 7
  private val branchOfEmployment = 8
  private val position = 9
  private val companyOwnership = 10
  private val relationshipToForeignCapital = 11
  private val directionOfActivity = 12
  private val familyIncome = 13

  if (args.length < 1) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: customersinterest <datapath>")
    System.exit(2)
  }

  private val featuresPath = args(0) + "Objects.csv"
  private val labelsPath = args(0) + "Target.csv"

  private val appName = "CustomersInterestModelSparkApp"
  private val masterUrl = System.getProperty("masterUrl")

  val conf = new SparkConf().setAppName(appName)
  LOG.info("SparkConfig created successfully")

  if (masterUrl != null && !masterUrl.eq("")) {
    conf.setMaster(masterUrl)
    LOG.info("Mater URL set as: \"" + masterUrl + "\"")
  }
  val sc = new SparkContext(conf)
  LOG.info("SparkContext created successfully")

  val features = sc.textFile(featuresPath)
    .map(createVector)
    .repartition(1)
  val labels = sc.textFile(labelsPath).repartition(1)
  val data = labels
    .zip(features)
    .map(tup => new LabeledPoint(tup._1.toDouble, tup._2))


  val auRoc = getRoc(data)
  println("RF Area under ROC = " + auRoc)

  def getRoc(dataset: RDD[LabeledPoint]): Double = {
    // split data to training (60%) and test (40%)
    val splits = dataset.randomSplit(Array(0.6, 0.4))

    val trainingData = splits(0).cache()
    val testData = splits(1).cache()

    // Train a RandomForest model.
    // Empty categoricalFeaturesInfo indicates all features are continuous.
    val categoricalFeaturesInfo = Map(
      isEmployed -> 2,
      isRetired -> 2,
      sex -> 2,
      education -> 7,
      maritalStatus -> 5,
      branchOfEmployment -> 31,
      position -> 12,
      companyOwnership -> 5,
      relationshipToForeignCapital -> 2,
      directionOfActivity -> 10,
      familyIncome -> 5
    )
    val numTrees = 120
    // Use more in practice. (sqrt, log2)
    val featureSubsetStrategy = "auto"
    // Let the algorithm choose.
    val impurity = "variance"
    val maxDepth = 4
    val maxBins = 50
    // Run Random Forest
    val model = RandomForest.trainRegressor(
      trainingData,
      categoricalFeaturesInfo,
      numTrees,
      featureSubsetStrategy,
      impurity,
      maxDepth,
      maxBins)

    // Compute raw scores on the test set.
    val scoreAndLabels = testData.map { point =>
      val score = model.predict(point.features)
      (score, point.label)
    }

    // Get evaluation metrics.
    val metrics = new BinaryClassificationMetrics(scoreAndLabels)
    metrics.areaUnderROC()
  }

  def createVector(string: String): Vector = {
    val split = string.replace(',', '.').split(';')
    val list = split.map(col => if (col.toLowerCase == "nan") 0.0 else col.toDouble)
    list(13) = list(13) - 1
    Vectors.dense(list)
  }
}
