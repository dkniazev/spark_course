package com.epam.mlmodels

import org.apache.log4j.Logger
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.feature.{ChiSqSelector, StandardScaler}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Dmitrii_Kniazev 
  * @since 12/14/2016
  */
object LogisticRegressionModel extends App {
  private val LOG: Logger = Logger.getLogger(LogisticRegressionModel.getClass)

  if (args.length < 1) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: customersinterest <datapath>")
    System.exit(2)
  }

  private val featuresPath = args(0) + "Objects.csv"
  private val labelsPath = args(0) + "Target.csv"

  private val appName = "CustomersInterestModelSparkApp"
  private val masterUrl = System.getProperty("masterUrl")

  val conf = new SparkConf().setAppName(appName)
  LOG.info("SparkConfig created successfully")

  if (masterUrl != null && !masterUrl.eq("")) {
    conf.setMaster(masterUrl)
    LOG.info("Mater URL set as: \"" + masterUrl + "\"")
  }
  val sc = new SparkContext(conf)
  LOG.info("SparkContext created successfully")

  val features = sc.textFile(featuresPath).map(createVector).repartition(1)
  val labels = sc.textFile(labelsPath).repartition(1)
  val data = labels
    .zip(features)
    .map(tup => new LabeledPoint(tup._1.toDouble, tup._2))

  val stdScaler = new StandardScaler().fit(data.map(x => x.features))

  //will be unit variance.
  val scaledData = data.map(lp =>
    LabeledPoint(lp.label, stdScaler.transform(lp.features)))

  val selector = new ChiSqSelector(20)
  // Create ChiSqSelector model (selecting features)
  val transformer = selector.fit(scaledData)
  // Filter the top 50 features from each feature vector
  val filteredData = scaledData.map { lp =>
    LabeledPoint(lp.label, transformer.transform(lp.features))
  }

  val auRoc = getRoc(data)
  println("LR (source) Area under ROC = " + auRoc)

  val auRocScaled = getRoc(scaledData)
  println("LR (scaled) Area under ROC = " + auRocScaled)

  val auRocFiltred = getRoc(filteredData)
  println("LR (filtred) Area under ROC = " + auRocFiltred)
  println("Features:" + transformer.selectedFeatures.length)
  println(transformer.selectedFeatures.mkString(","))

  def getRoc(dataset: RDD[LabeledPoint]): Double = {
    // split data to training (60%) and test (40%)
    val splits = dataset.randomSplit(Array(0.6, 0.4))

    val trainingData = splits(0).cache()
    val testData = splits(1).cache()
    // Run Logistic Regression using the SGD algorithm.
    val model = LogisticRegressionWithSGD.train(trainingData, 110, 1.1)

    // Clear the default threshold.
    model.clearThreshold()

    // Compute raw scores on the test set.
    val scoreAndLabels = testData.map { point =>
      val score = model.predict(point.features)
      (score, point.label)
    }

    // Get evaluation metrics.
    val metrics = new BinaryClassificationMetrics(scoreAndLabels)
    metrics.areaUnderROC()
  }

  //TODO add Feature hashing
  def createVector(string: String): Vector = {
    val split = string.replace(',', '.').split(';')
    val list = split.map(col => if (col.toLowerCase == "nan") 0.0 else col.toDouble)
    list(13) = list(13) - 1
    Vectors.dense(list)
  }
}
